import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NuevoAlumnoComponent } from './nuevo-alumno/nuevo-alumno.component';
import { CustomMaterialModule } from './CustomMaterial.module';

import { GeneroPipe } from './genero.pipe';

import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    AppComponent,
    NuevoAlumnoComponent,
    GeneroPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    Ng2SearchPipeModule
  ],
  providers: [],
  exports:[NuevoAlumnoComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
