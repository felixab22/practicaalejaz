export class EstudianteModel
{
    id?: any;
    name?: string;
    gender?: string;
    description?: number;
    serie?: string;
    correo?: string;
    codigo?: string;
 
    /**
     * Constructor
     *
     * @param contact
     */
    // constructor(contact:any)
    // {
    //     {
    //         this.id = contact.id || 1;
    //         this.name = contact.name || '';
    //         this.gender = contact.gender || '';
    //         this.description = contact.description || '';
    //         this.serie = contact.serie || '';
    //         this.correo = contact.correo || '';
    //         this.codigo = contact.codigo || '';
    //     }
    // }
}