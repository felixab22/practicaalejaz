import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NuevoAlumnoComponent } from './nuevo-alumno/nuevo-alumno.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  verimagen = true;
  imagen = './assets/img/fondo1.jpg' || "/../assets/img/fondo1.jpg";

  currentCategory = 'ALL';
  filteralumnos: any[];
  listarAlumnos: any[];
  term = '';
  series = [
    { value: '100', description: '100' },
    { value: '200', description: '200' },
    { value: '300', description: '300' },
    { value: '400', description: '400' },
    { value: '500', description: '500' },
  ]
  idgenered = 2;

  titulo = "LISTA DE ALUMNOS DE INGENIERIA DE SISTEMAS"
  alumnoAll = [
    {
      id: '312122hgsdf',
      name: 'Felix ASTO BERROCAL',
      gender: 'v',
      description: 'Alumno de la escuela de sistemas',
      serie: '500',
      correo: 'felix.asto.27@unsch.edu.pe',
      codigo: '46288952'
    },
    {
      id: '312122dfteq',
      name: 'Hever FERNANDEZ GONZALEZ',
      gender: 'v',
      description: 'Alumno alegre',
      serie: '400',
      correo: 'hever.fernandez.27@unsch.edu.pe',
      codigo: '27085791'
    },
    {
      id: '312122dfteq',
      name: 'MARIANA GONZALES RAMIREZ',
      gender: 'm',
      description: 'Alumno alegre',
      serie: '300',
      correo: 'mariana.gonzales.27@unsch.edu.pe',
      codigo: '27155791'
    }
  ]
  constructor(
    public dialog: MatDialog,
  ) {
    this.listarAlumnos = this.filteralumnos = this.alumnoAll;
  }
  ngOnInit() {
    this.setDelay()
  }

  nuevoPracticaModel(valor?: any) {
    var entrada = 'edit';
    if (valor === undefined || valor === null) {
      entrada = 'new';
    }
    const dialogRef = this.dialog.open(NuevoAlumnoComponent, {
      width: '550px',
      height: '650px',
      data: {
        contact: valor,
        action: entrada
      }
    });

    dialogRef.afterClosed().subscribe((response: any) => {
      console.log(response);

      if (!response) {
        return;
      }
      const actionType: string = response[0];
      const formData: any = response[1];
      switch (actionType) {
        case 'save':
          this.alumnoAll.unshift(formData);
          break;
        case 'edit':
          this.editList(formData)
          break;
        case 'delete':
          this.deleteAlumno(formData.id)
          break;
      }
    });
  }


  public deleteAlumno(id: string): void {
    if (confirm('¿Está seguro que desea eliminar el dato?')) {
      this.alumnoAll = this.alumnoAll.filter(u => id !== u.id);
      this.filteralumnos = this.listarAlumnos = this.alumnoAll;
    }
  }
  editList(item: any) {

    this.alumnoAll = this.alumnoAll.filter(u => item.id !== u.id);
    this.alumnoAll.unshift(item);
    this.filteralumnos = this.listarAlumnos = this.alumnoAll;

  }
  filterAlumnoAllByCategory(): void {
    // Filter
    if (this.currentCategory === 'all') {
      this.listarAlumnos = this.alumnoAll;
      this.filteralumnos = this.alumnoAll;
    }
    else {
      this.listarAlumnos = this.alumnoAll.filter((alumno) => {
        return alumno.serie === this.currentCategory;
      });

      this.filteralumnos = [...this.listarAlumnos];

    }
  }
  setDelay() {
    setTimeout(() => {
      this.verimagen = false;
    }, 3500);
  }
}
