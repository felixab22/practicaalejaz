import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatDialogModule} from '@angular/material/dialog';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';

@NgModule({
    declarations: [],
  
    imports: [
      CommonModule,
      MatDialogModule,
      MatToolbarModule,
      MatIconModule,
      MatInputModule,
      MatSelectModule,
      MatRadioModule,
      MatBottomSheetModule,
      MatButtonToggleModule
    ],
    exports: [
     MatDialogModule,
     MatToolbarModule,
     MatIconModule,
     MatInputModule,
     MatSelectModule,
     MatRadioModule,
     MatButtonModule,
     MatBottomSheetModule,
      MatButtonToggleModule
    ],
  })
  export class CustomMaterialModule { }
  