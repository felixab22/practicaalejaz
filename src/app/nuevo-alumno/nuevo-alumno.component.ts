import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EstudianteModel } from '../estudiante.model';

@Component({
  selector: 'app-nuevo-alumno',
  templateUrl: './nuevo-alumno.component.html',
  styleUrls: ['./nuevo-alumno.component.scss']
})
export class NuevoAlumnoComponent implements OnInit {
  action: string;
  idgenerado = '';
  contact = new EstudianteModel();
  dialogTitle = 'NUEVO';
  validatingForm : any;
  genero = [
    {value:'v', description:'VARON'},
    {value:'m', description:'MUJER'}
]
series = [
  {value:'100', description:'100'},
  {value:'200', description:'200'},
  {value:'300', description:'300'},
  {value:'400', description:'400'},
  {value:'500', description:'500'},
]
  // mostrar = false;;
  constructor(
    @Inject(MAT_DIALOG_DATA) public _data: any,
    private _formBuilder: FormBuilder,
    public matDialogRef: MatDialogRef<NuevoAlumnoComponent>,
  ) {    
    this.action = _data.action;
    if (this.action === 'edit') {
      this.dialogTitle = 'EDITAR EDUCACIÓN';
      this.contact = _data.contact;
      this.validatingForm = this.createVarlidatortForm(this.contact);
      
    }
    else {
      this.dialogTitle = 'MI EDUCACIÓN';
      this.idgenerado = Math.random().toString(26).slice(2);
      this.newValidatorForm();
    }
   }

  ngOnInit(): void {
    this.idgenerado = Math.random().toString(26).slice(2);    
  }
  newValidatorForm() {
    this.validatingForm = this._formBuilder.group({
      name: ['', Validators.required],
      gender: ['', Validators.required],
      description: ['', Validators.required],
      serie: ['', Validators.required],
      correo: ['', Validators.email],
      codigo: ['', Validators.required,Validators.maxLength(8)],
      id:[this.idgenerado]
    });

  }
  createVarlidatortForm(contact?:any): any {
    if(contact) {
      return this._formBuilder.group({
        name: [contact.name],
        gender: [contact.gender],
        description: [contact.description],
        serie: [contact.serie],
        correo: [contact.correo],
        codigo: [contact.codigo],
        id: [contact.id]
      });
    }
    else{
      console.log('vacio');
      
    }
  }

}
