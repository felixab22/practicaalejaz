import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'genero'})
export class GeneroPipe implements PipeTransform {
    transform(value: any): any {
        if(value === 'v'){
            return 'VARON'
        }
        return 'FEMENINO'
    }
}